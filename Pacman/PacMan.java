import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

import java.util.List;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 * PacMan. A classic PacMan game made on Greenfoot.
 * 
 * @author Otávio Cesário e Jefferson Gandra
 * @version 1.0.0
 */
public class PacMan extends Actor implements Movimento
{
    private int foodEaten;
    private int direction;
    private boolean started = false;

    public PacMan()
    {
        setDirection(EAST);
        foodEaten = 0;
        getImage().scale(35,35);
    }

    /**
     * Do whatever the PacMan likes to to just now.
     */
    public void act()
    {
        if(started == false){
            JOptionPane.showMessageDialog(null, "Get Ready!", "Pacman say: ", JOptionPane.INFORMATION_MESSAGE);
            while(!(Greenfoot.isKeyDown("left") || Greenfoot.isKeyDown("right") || Greenfoot.isKeyDown("up") || Greenfoot.isKeyDown("down"))){
                
            }
            started = true;
        }else{
        
            if(foundComida() == true) {
                eatComida();
            }
            
            if(Greenfoot.isKeyDown("left")){
                direction = WEST;
                move();
            }else if(Greenfoot.isKeyDown("right")){
               direction = EAST;
               move();
            }else if(Greenfoot.isKeyDown("up")){
                direction = NORTH;
                move();
            }else if(Greenfoot.isKeyDown("down")){
                direction = SOUTH;
                move();
            }else{
                move();
            }
        }
    }
    
    /**
     * Check whether there is a Comida in the same cell as we are.
     */
    public boolean foundComida()
    {
        Actor Comida = getOneObjectAtOffset(0, 0, Comida.class);
        if(Comida != null) {
            eatComida();
            return true;
        }
        else {
            return false;
        }
    }
       
    /**
     * Move one cell forward in the current direction.
     */
    public void move()
    {
        if (!canMove()) {
            return;
        }
        switch(direction) {
            case SOUTH :
                setLocation(getX(), getY() + 1);
                break;
            case EAST :
                setLocation(getX() + 1, getY());
                break;
            case NORTH :
                setLocation(getX(), getY() - 1);
                break;
            case WEST :
                setLocation(getX() - 1, getY());
                break;
        }
        
        setDirection(direction);
    }
    
    
    // Verifica se há muro na direção em que os movimentadores seguem
    public boolean foundMuro()
    {
        int x = 0;
        int y = 0;
        
        switch(direction) {
            case SOUTH :
                y++;
                break;
            case EAST :
                x++;
                break;
            case NORTH :
                y--;
                break;
            case WEST :
                x--;
                break;
        }
        
        Actor muro = getOneObjectAtOffset(x, y, Muro.class);
        
       if(muro != null) {
            return true;
       }
       else {
           return false;
       }
    }
    
    /**
     * Test if we can move forward. Return true if we can, false otherwise.
     */
    public boolean canMove()
    {
        World myWorld = getWorld();
        int x = getX();
        int y = getY();
        switch(direction) {
            case SOUTH :
                y++;
                break;
            case EAST :
                x++;
                break;
            case NORTH :
                y--;
                break;
            case WEST :
                x--;
                break;
        }
       
        // test for outside border
        if (x >= myWorld.getWidth() || y >= myWorld.getHeight()) {
            return false;
        }
        else if (x < 0 || y < 0) {
            return false;
        }else if(foundMuro()){
            return false;
        }
        return true;
    }
    
     /**
     * Sets the direction we're facing.
     */
    public void setDirection(int direction)
    {
        this.direction = direction;
        switch(direction) {
            case SOUTH :
                setRotation(90);
                break;
            case EAST :
                setRotation(0);
                break;
            case NORTH :
                setRotation(270);
                break;
            case WEST :
                setRotation(180);
                break;
            default :
                break;
        }
    }
    
    /**
     * Eat a Comida.
     */
    public void eatComida()
    {
        Actor Comida = getOneObjectAtOffset(0, 0, Comida.class);
        if(Comida != null) {
            // eat the Comida...
            getWorld().removeObject(Comida);
            foodEaten = foodEaten + 20; 
        }
    }
    
    /**
     * Turns towards the left.
     */
    public void turnLeft()
    {
        switch(direction) {
            case SOUTH :
                setDirection(EAST);
                break;
            case EAST :
                setDirection(NORTH);
                break;
            case NORTH :
                setDirection(WEST);
                break;
            case WEST :
                setDirection(SOUTH);
                break;
        }
    }
    
    /**
     * Scoreboard.
     */
    public int getfoodEaten()
    {
        return foodEaten;
    }
}