import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.Random;
import javax.swing.JOptionPane;

/**
 * Write a description of class Fantasma here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Fantasma extends Actor implements Movimento
{
    private int direction;
    public Fantasma(){
        getImage().scale(40, 40);
    }

    /**
     * Act - do whatever the Fantasma wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {   
        Random rnd = new Random();
        
        if(canMove()){
            move();
            direction = rnd.nextInt(4) % 4;
        }
        
        if(getOneObjectAtOffset(0, 0, PacMan.class) != null){
            JOptionPane.showMessageDialog(null, "You lose, Bro\n\nFinal Score: " + 
               ((PacManWorld)getWorld()).getPacMan().getfoodEaten(), "GAME OVER", JOptionPane.INFORMATION_MESSAGE);
             
            try{
                Greenfoot.stop();
            }catch(Exception e){
                
            }
        }
        else{
            direction = rnd.nextInt(4) % 4;
            turnLeft();
        }
    }   
      
    /**
     * Move one cell forward in the current direction.
     */
    public void move()
    {
        if (!canMove()) {
            return;
        }
        switch(direction) {
            case SOUTH :
                setLocation(getX(), getY() + 1);
                break;
            case EAST :
                setLocation(getX() + 1, getY());
                break;
            case NORTH :
                setLocation(getX(), getY() - 1);
                break;
            case WEST :
                setLocation(getX() - 1, getY());
                break;
        }
        
        setDirection(direction);
    }
   
    // Verifica se há muro na direção em que os movimentadores seguem
    public boolean foundMuro()
    {
        int x = 0;
        int y = 0;
        
        switch(direction) {
            case SOUTH :
                y++;
                break;
            case EAST :
                x++;
                break;
            case NORTH :
                y--;
                break;
            case WEST :
                x--;
                break;
        }
        
        Actor muro = getOneObjectAtOffset(x, y, Muro.class);
        
       if(muro != null) {
            return true;
       }
       else {
           return false;
       }
    }
    
    /**
     * Test if we can move forward. Return true if we can, false otherwise.
     */
    public boolean canMove()
    {
        World myWorld = getWorld();
        int x = getX();
        int y = getY();
        switch(direction) {
            case SOUTH :
                y++;
                break;
            case EAST :
                x++;
                break;
            case NORTH :
                y--;
                break;
            case WEST :
                x--;
                break;
        }
       
        // test for outside border
        if (x >= myWorld.getWidth() || y >= myWorld.getHeight()) {
            return false;
        }
        else if (x < 0 || y < 0) {
            return false;
        }else if(foundMuro()){
            return false;
        }
        return true;
    }
    
      /**
     * Sets the direction we're facing.
     */
    public void setDirection(int direction)
    {
        this.direction = direction;
        switch(direction) {
            case SOUTH :
                setRotation(90);
                break;
            case EAST :
                setRotation(0);
                break;
            case NORTH :
                setRotation(270);
                break;
            case WEST :
                setRotation(180);
                break;
            default :
                break;
        }
    }
    
     /**
     * Turns towards the left.
     */
    public void turnLeft()
    {
        switch(direction) {
            case SOUTH :
                setDirection(EAST);
                break;
            case EAST :
                setDirection(NORTH);
                break;
            case NORTH :
                setDirection(WEST);
                break;
            case WEST :
                setDirection(SOUTH);
                break;
        }
    }
}
