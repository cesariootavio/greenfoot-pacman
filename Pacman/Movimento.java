/**
 * Write a description of class Movimento here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public interface Movimento
{
    static final int EAST = 0;
    static final int WEST = 1;
    static final int NORTH = 2;
    static final int SOUTH = 3;
   
    public void move();
    
    public boolean foundMuro();
    
    public boolean canMove();
    
    public void setDirection(int direction);
    
    public void turnLeft();
}
