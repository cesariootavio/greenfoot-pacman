import greenfoot.*;  // imports Actor, World, Greenfoot, GreenfootImage
import java.util.Random;
import javax.swing.JOptionPane;

/**
 * A world where PacMans live.
 * 
 * @author Michael Kolling
 * @version 1.0.1
 */
public class PacManWorld extends World
{
    public PacMan pacMan;
    
    /**
     * Create a new world with 8x8 cells and
     * with a cell size of 60x60 pixels
     */
    public PacManWorld() 
    {
        super(10, 10,60);        
        populate();
    }

    /**
     * Populate the world with a fixed scenario of PacMans and leaves.
     */    
    public void populate()
    {
        pacMan = new PacMan();
        addObject(pacMan, 3, 3);

        createMuroEFantasma();
        randomComida();        
    }
    
    public void randomComida()
    {
        Comida c;    
         for(int i=0; i<10; i++){
            for(int j=0; j<10; j++){        
                    if(getObjectsAt(i, j, PacMan.class).size() == 0 && 
                        getObjectsAt(i, j, Fantasma.class).size() == 0 &&
                            getObjectsAt(i, j, Muro.class).size() == 0)
                            {
                               c = new Comida();    
                               addObject(c, i, j);
                            }
            }
        }
    }
    
    private void createMuroEFantasma()
    {
        
        //2 = Fantasma e 1 = Muro
        Muro l4;
        Fantasma l5;
        int [][]muros ={{2,0,0,1,1,0,0,0,0,2},
                        {0,0,0,0,0,0,0,0,1,0},
                        {0,1,1,0,1,1,0,1,0,0},
                        {0,0,0,0,0,0,0,0,0,1},
                        {0,0,1,0,0,1,0,1,0,1},
                        {0,1,0,0,0,0,0,1,0,1},
                        {0,0,0,0,1,1,1,1,0,0},
                        {0,0,0,1,0,0,0,0,0,0},
                        {0,1,0,0,0,1,0,1,1,0},
                        {2,0,0,0,0,0,0,0,0,2}};
                        
        for(int i=0; i<10; i++){
            for(int j=0; j<10; j++){
                if(muros[i][j] == 1){
                    l4 = new Muro();
                    addObject(l4, i, j);
                }else
                    if(muros[i][j] == 2){
                        l5 = new Fantasma();
                        addObject(l5, i, j);
                }
            }
        }
    }
    
    /**
     * Return pacMan object
     */
    public PacMan getPacMan()
    {
        return pacMan;
    }
}