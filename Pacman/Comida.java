import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * Comida - a class for representing Comidas.
 *
 * @author Michael Kolling
 * @version 1.0.1
 */
public class Comida extends Actor
{
    public Comida()
    {
        getImage().scale(20, 20);
    }
}